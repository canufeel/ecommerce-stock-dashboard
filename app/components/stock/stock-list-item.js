import Ember from 'ember';

export default Ember.Component.extend({
	store: Ember.inject.service(),
	goods: Ember.computed('model', {
		get(){
			let store = this.get('store');
			let goods = store.findAll('good');
			return goods;
		}
	}),
	resolveGoodLabel(model){
		if (!Ember.isBlank(model.get('content'))){
			return model.get('name');
		} else {
			return '';
		}
	},
	unitCost: Ember.computed('model.quantity', 'model.cost', {
		get(){
			let quantity = this.get('model.quantity');
			let cost = this.get('model.cost');
			if (Ember.isBlank(cost) || Ember.isBlank(quantity)){
				return '';
			}
			let unitCost = cost/quantity;
			return unitCost;
		},
		set(name,currStr,previous){
			let quantity = this.get('model.quantity');
			this.set('model.cost', quantity*parseInt(currStr));
		}
	}),
	actions: {
		save(){
			this.attrs.save(this.get('model'));
		},
		delete(){
			this.attrs.delete(this.get('model'));
		}		
	}
});
