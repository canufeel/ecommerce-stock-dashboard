import Ember from 'ember';

export default Ember.Component.extend({
	store: Ember.inject.service(),
	actions: {
		new(){
			let store = this.get('store');
			store.createRecord('stock');
		},
		save(item){
			let promise = item.save();
		},
		delete(item){
			item.destroyRecord();
		}
	}
});
