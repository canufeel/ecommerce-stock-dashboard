import Ember from 'ember';

export default Ember.Component.extend({
	routing: Ember.inject.service('-routing'),
	actions: {
		transition(to){
			let routing = this.get('routing');
			routing.transitionTo(to);
		}
	}
});
