import Ember from 'ember';

export default Ember.Component.extend({
	store: Ember.inject.service(),
	goods: Ember.computed('model', {
		get(){
			let store = this.get('store');
			let goods = store.peekAll('good');
			return goods;
		}
	}),
	resolveGoodLabel(model){
		if (!Ember.isBlank(model.get('content'))){
			return model.get('reason');
		} else {
			return '';
		}
	},
	actions: {
		save(){
			this.attrs.save(this.get('model'));
		},
		delete(){
			this.attrs.delete(this.get('model'));
		}		
	}
});
