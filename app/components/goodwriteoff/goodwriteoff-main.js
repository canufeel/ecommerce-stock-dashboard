import Ember from 'ember';
import StockMain from 'front/components/stock/stock-main';

export default StockMain.extend({
	actions: {
		new(){
			let store = this.get('store');
			store.createRecord('goodwriteoff');
		}
	}
});
