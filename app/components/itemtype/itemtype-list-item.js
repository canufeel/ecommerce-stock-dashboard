import Ember from 'ember';

export default Ember.Component.extend({
	store: Ember.inject.service(),
	resolveGoodLabel(model){
		if (!Ember.isBlank(model.get('content'))){
			return model.get('name');
		} else {
			return '';
		}
	},
	actions: {
		save(){
			this.attrs.save(this.get('model'));
		},
		delete(){
			this.attrs.delete(this.get('model'));
		},
		refreshGroups(){
			this.attrs.refreshAllGroups();
		}
	},
	totalCost: Ember.computed('allItemsCount','model.cost',{
		get(){
			return this.get('allItemsCount') * this.get('model.cost');
		}
	})
});
