import Ember from 'ember';

export default Ember.Component.extend({
	store: Ember.inject.service(),
	checkBoxValue: false,
	checkBoxValueEverSet: false,
	createNewGroup(good, quantity){
		let store = this.get('store');

		let group = store.createRecord('goodgroup',{
			quantity: quantity,
			good:good
		});

		group = group.save();
		return group.then((instance)=>{
			this.attrs.refreshAllGroups();
			return instance;
		});
	},
	findGroup(good,quantity,value){
		let allGroups = this.get('allGroups');
		let group;
		// we check if we have group
		// like this in the persistance
		// layer.
		let same = allGroups.filter((gr)=>{
			return Ember.isEqual(gr.get('good.id'),good.get('id')) && Ember.isEqual(gr.get('quantity'),quantity);
		});
		if (same.length > 0){
			group = same[0];
			this.set('group',group);
			Ember.run.scheduleOnce('afterRender',this,()=>{
				this.unpromiseGroup(value,group);
			});
		} else {
			//if not we create a new one
			Ember.run.scheduleOnce('afterRender', this, ()=>{
				group = this.createNewGroup(good,quantity);
				this.unpromiseGroup(value,group);
			});
		}
	},
	unpromiseGroup(value,group){
		if (group.then){
			group.then((result)=>{
				this.pushToItemType(value,result);
			});
		} else {
			this.pushToItemType(value,group);
		}
	},
	pushToItemType(value,group){
		let goodgroups = this.get('goodgroups');
		if (value){
			goodgroups.pushObject(group);
		} else {
			goodgroups.removeObject(group);
		}
	},
	currentGood: Ember.computed('_item',{
		get(){
			return null;
		},
		set(param,value){
			if (!Ember.isBlank(value)){
				this.findGroup(value,1,true);
			}
		}
	}),
	actions: {
		update(){
			this.pushToItemType(...arguments);
		},
		find(){
			this.findGroup(...arguments);
		}
	}
});
