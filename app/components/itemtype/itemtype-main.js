import Ember from 'ember';
import pagedArray from 'ember-cli-pagination/computed/paged-array';

export default Ember.Component.extend({

	store: Ember.inject.service(),
	actions: {
		new(){
			let store = this.get('store');
			store.createRecord('itemtype');
		},
		save(item){
			let promise = item.save();
		},
		delete(item){
			item.destroyRecord();
		},
		refreshGroups(){
			let store = this.get('store');
			this.set('allGroups',store.peekAll('goodgroup'));
		}
	},
	queryParams: ["page", "perPage"],
	page: 1,
  	perPage: 10,
  	pagedContent: pagedArray('model', {pageBinding: "page", perPageBinding: "perPage"}),
  	totalPagesBinding: "pagedContent.totalPages"
});
