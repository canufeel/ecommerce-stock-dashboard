import Ember from 'ember';

export default Ember.Component.extend({
	selectedGoodQuantity: Ember.computed('selectedGood','_selectedQuantity', {
		get(){
			if (Ember.isBlank(this.get('_selectedQuantity'))){
				let quantity = this.get('selectedGood.quantity');
				this.set('_selectedQuantity',quantity);
			}
			return this.get('_selectedQuantity');
		},
		set(param,val){
			val = isNaN(parseInt(val)) ? 0 : parseInt(val);
			this.set('_selectedQuantity',val);
			return val;
		}
	}),
	actions: {
		deleteSelected(goodgroup){
			this.attrs.update(false,goodgroup);
		},
		saveSelected(goodgroup){
			let good = goodgroup.get('good');
			let quantity = this.get('selectedGoodQuantity');
			this.attrs.update(false,goodgroup);
			this.attrs.find(good,quantity,true);
		}
	}
});
