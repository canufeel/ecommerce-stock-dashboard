import Ember from 'ember';
const {computed,get} = Ember;

export default Ember.Component.extend({
	completionPercentage: computed('model',{
		get(){
			let total = get(this,'model.total_orders_count');
			let completed = get(this,'model.completed_orders_count');
			return Math.round(completed/total * 10000)/100;
		}
	}),
	completionMoneyPercentage: computed('model',{
		get(){
			let total = get(this,'model.total_orders_amount');
			let completed = get(this,'model.revenue');
			return Math.round(completed/total * 10000)/100;	
		}
	}),
	grossMargin: computed('model',{
		get(){
			let revenue = get(this,'model.revenue');
			let cogs = get(this,'model.cost');
			return Math.round((revenue - cogs) / revenue * 10000)/100;
		}
	}),
	grossMarginFull: computed('model',{
		get(){
			let revenue = get(this,'model.revenue') + get(this,'model.writeoff_revenue');
			let cogs = get(this,'model.cost') + get(this,'model.writeoff_cogs');
			return Math.round((revenue - cogs) / revenue * 10000)/100;
		}
	}),
	actions:{
		select(item){
			this.attrs.select(item);
		}
	}
});
