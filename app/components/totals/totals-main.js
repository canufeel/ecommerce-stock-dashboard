import Ember from 'ember';
const {computed,get,set,isEqual} = Ember;

export default Ember.Component.extend({
	_selected: false,
	isSelected: computed('_selected',{
		get(){
			return get(this,'_selected');
		}
	}),
	selectedYear: computed('_selectedYear',{
		get(){
			let years = get(this,'model.years');
			let selectedYear = get(this,'_selectedYear');
			let switch_to = years.find((year)=>{
				return isEqual(selectedYear,year.get('number'));
			});
			return switch_to;
		}
	}),
	actions: {
		selectYear(year){
			set(this,'_selectedYear',year);
			set(this,'_selected',true);
		},
		selectMonth(month){

		}
	}
});
