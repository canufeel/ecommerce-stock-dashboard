import Ember from 'ember';
const {computed, get, A} = Ember;

/* global d3 */

export default Ember.Component.extend({
	barData: computed('model',{
		get(){
			let model = get(this,'model');
			let year = get(this,'year.number');
			let resultArray = new A();
			model.forEach((item)=>{
				resultArray.pushObjects([
				{
					time:d3.time.format('%Y-%m-%d').parse(`${year}-${item.get('number')}-1`),
					label:'total orders',
					value:item.get('total_orders_amount'),
					type:'money'
				},
				{
					time:d3.time.format('%Y-%m-%d').parse(`${year}-${item.get('number')}-1`),
					label:'revenue',
					value:item.get('revenue'),
					type:'money'
				},
				{
					time:d3.time.format('%Y-%m-%d').parse(`${year}-${item.get('number')}-1`),
					label:'cost',
					value:item.get('cost'),
					type:'money'
				},
				{
					time:d3.time.format('%Y-%m-%d').parse(`${year}-${item.get('number')}-1`),
					label:'profit',
					value:item.get('profit'),
					type:'money'
				}
				]);
			});
			return resultArray;
		}
	}),
	lineData: computed('model',{
		get(){
			let model = get(this,'model');
			let year = get(this,'year.number');
			let resultArray = new A();
			model.forEach((item)=>{
				resultArray.pushObjects([
				{
					time:d3.time.format('%Y-%m-%d').parse(`${year}-${item.get('number')}-1`),
					label:'To revenue conv., %',
					value:item.get('revenueConv'),
					type:'money'
				},
				{
					time:d3.time.format('%Y-%m-%d').parse(`${year}-${item.get('number')}-1`),
					label:'Gross Margin, %',
					value:item.get('grossMargin'),
					type:'money'
				},
				{
					time:d3.time.format('%Y-%m-%d').parse(`${year}-${item.get('number')}-1`),
					label:'Order Completion, %',
					value:item.get('completionPercentage'),
					type:'money'
				}
				]);
			});
			return resultArray;
		}
	})
});
