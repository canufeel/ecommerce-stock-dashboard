import Ember from 'ember';
import ENV from 'front/config/environment';

/* global d3 */

const APP = ENV.APP;

export default Ember.Component.extend({
	displayData: Ember.computed('goods',{
		get(){
			let goods = this.get('goods');
			let promise = new Ember.RSVP.Promise((resolve)=>{
				let good_q = ""
				goods.forEach((good)=>{
					good_q += `${good.get('id')},`;
				});
				good_q = good_q.slice(0,good_q.length-1);
				let request = Ember.$.getJSON(`${APP.API_HOST}/${APP.API_NAMESPACE}/goodgroupsgraph?good=${good_q}&status=5`);
				request.then((response)=>{
					response.forEach((item)=>{
						item.time = d3.time.format('%Y-%m-%d').parse(item.time);
					});
					resolve(response);
				});
			});

			let ObjectPromiseProxy = Ember.ObjectProxy.extend(Ember.PromiseProxyMixin);

			let proxy = ObjectPromiseProxy.create({
			  	promise: promise
			});

			return proxy;
		}
	})
});
