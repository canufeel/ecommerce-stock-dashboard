import Ember from 'ember';

export default Ember.Component.extend({
	_isChart: false,
	_showGoods: Ember.A(),
	showGoods: Ember.computed.alias('_showGoods'),
	isChart: Ember.computed.alias('_isChart'),
	store: Ember.inject.service(),
	actions: {
		new(){
			let store = this.get('store');
			store.createRecord('good');
		},
		recalculate(){
			debugger;
		},
		showToggle(good,value){
			if (value) {
				this.get('_showGoods').pushObject(good);
			} else {
				this.get('_showGoods').removeObject(good);
			}
		},
		showSelected(){
			this.toggleProperty('_isChart');
		}
	}
});
