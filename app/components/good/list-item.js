import Ember from 'ember';

export default Ember.Component.extend({
	store: Ember.inject.service(),
	currentItemStocks: Ember.computed('good',{
		get(){
			let good = this.get('good');
			let store = this.get('store');
			let stocks = store.query('stock',{'good':good.get('id')});
			return stocks;
		}
	}),
	currentStock: Ember.computed('currentItemStocks',{
		get(){
			let currentItemStocks = this.get('currentItemStocks');
			let total = 0;
			let promise = new Ember.RSVP.Promise((resolve)=>{
				currentItemStocks.then((cureentItemUnpromisedStocks)=>{
					cureentItemUnpromisedStocks.forEach((stock)=>{
						total += stock.get('left');
					});
					resolve(total);
				});
			});

			var ObjectPromiseProxy = Ember.ObjectProxy.extend(Ember.PromiseProxyMixin);

			var proxy = ObjectPromiseProxy.create({
			  promise: promise
			});
			return proxy;
		}
	}),
	itemChecked: Ember.computed('_itemChecked',{
		get(){
			if (!Ember.isBlank(this.get('_itemChecked'))){
				return this.get('_itemChecked');
			} else {
				return false;
			}
		},
		set(param,value){
			this.set('_itemChecked',value);
			this.attrs.showToggle(this.get('good'),value);
			return value;
		}
	}),
	actions: {
		save(){
			let good = this.get('good');
			let result = good.save();
			result.then(()=>{
				let update = this.get('update');
				if (!Ember.isBlank(update)){
					this.sendAction('update');
				}
			})
		},
		delete(){
			let good = this.get('good');
			good.destroyRecord();
		}
	}
});
