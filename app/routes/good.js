import Ember from 'ember';

export default Ember.Route.extend({
	model(){
		return Ember.RSVP.hash({
			goods: this.store.findAll('good'),
			items: this.store.findAll('item'),
			stocks: this.store.findAll('stock')
		});
	}
});
