import Ember from 'ember';

export default Ember.Route.extend({
	model(){
		return Ember.RSVP.hash({
			orders: this.store.findAll('order'),
			months: this.store.findAll('month'),
			years: this.store.findAll('year')
		})
	}
});
