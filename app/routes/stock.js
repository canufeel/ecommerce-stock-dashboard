import Ember from 'ember';

export default Ember.Route.extend({
	model(){
		return Ember.RSVP.hash({
			stocks:this.store.findAll('stock'),
			goods:this.store.findAll('good')
		})
	}
});
