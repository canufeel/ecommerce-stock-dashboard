import Ember from 'ember';

export default Ember.Route.extend({
	model(){
		return Ember.RSVP.hash({
			goodwriteoff: this.store.findAll('goodwriteoff'),
			goods: this.store.findAll('good')
		}); 
	}
});
