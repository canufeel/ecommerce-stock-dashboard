import Ember from 'ember';

export default Ember.Route.extend({
	model(){
		return Ember.RSVP.hash({
			itemtype:this.store.findAll('itemtype'),
			goods:this.store.findAll('good'),
			items:this.store.findAll('item'),
			groups: this.store.findAll('goodgroup')
		})
	}
});
