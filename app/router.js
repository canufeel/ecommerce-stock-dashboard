import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.route('stock');
  this.route('good');
  this.route('totals');
  this.route('itemtype');
  this.route('goodwriteoff');
});

export default Router;
