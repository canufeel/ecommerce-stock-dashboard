import DS from 'ember-data';
import Ember from 'ember';
const {get,computed} = Ember;
const { attr, hasMany } = DS;

export default DS.Model.extend({
  number: attr('number'),
  orders: hasMany('order'),
  completed_orders_count: attr('number'),
  total_orders_count: attr('number'),
  cost: attr('number'),
  revenue: attr('number'),
  profit: attr('number'),
  total_orders_amount: attr('number'),
  writeoff_revenue: attr('number'),
  writeoff_cogs: attr('number'),
  completionPercentage: computed('total_orders_count','completed_orders_count',{
  	get(){
  		let total = get(this,'total_orders_count');
		let completed = get(this,'completed_orders_count');
		return Math.round(completed/total * 10000)/100;
  	}
  }),
  grossMargin: computed('revenue','cost',{
  	get(){
  		let revenue = get(this,'revenue');
		let cogs = get(this,'cost');
		return Math.round((revenue - cogs) / revenue * 10000)/100;
  	}
  }),
  revenueConv: computed('revenue','total_orders_amount',{
  	get(){
  		let total = get(this,'total_orders_amount');
		let completed = get(this,'revenue');
		return Math.round(completed/total * 10000)/100;
  	}
  })
});
