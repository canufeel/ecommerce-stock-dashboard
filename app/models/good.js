import DS from 'ember-data';

export default DS.Model.extend({
  name: DS.attr('string'),
  total: DS.attr('number'),
  sold: DS.attr('number'),
  left: DS.attr('number')
});
