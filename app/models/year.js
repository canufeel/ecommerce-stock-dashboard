import DS from 'ember-data';
const { attr, hasMany } = DS;

export default DS.Model.extend({
  number: attr('number'),
  months: hasMany('month'),
  completed_orders_count: attr('number'),
  total_orders_count: attr('number'),
  cost: attr('number'),
  revenue: attr('number'),
  profit: attr('number'),
  total_orders_amount: attr('number'),
  writeoff_revenue: attr('number'),
  writeoff_cogs: attr('number')
});
