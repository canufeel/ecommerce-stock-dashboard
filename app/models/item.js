import DS from 'ember-data';

export default DS.Model.extend({
  price: DS.attr('number'),
  item_type: DS.belongsTo('itemtype')
});
