import DS from 'ember-data';

export default DS.Model.extend({
  good: DS.belongsTo('good'),
  quantity: DS.attr('number'),
  cost: DS.attr('number'),
  date_added: DS.attr('xdate'),
  left: DS.attr('number')
});
