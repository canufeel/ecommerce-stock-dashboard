import DS from 'ember-data';

export default DS.Model.extend({
  good:DS.belongsTo('good'),
  quantity: DS.attr('number'),
  date: DS.attr('xdate'),
  reason:DS.attr('string'),
  sell_price:DS.attr('number'),
  cogs:DS.attr('number', {readOnly:true})
});
