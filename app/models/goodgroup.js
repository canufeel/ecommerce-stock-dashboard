import DS from 'ember-data';

export default DS.Model.extend({
  good: DS.belongsTo('good'),
  quantity: DS.attr('number')
});
