import Ember from 'ember';

export function getItemsCount([allItems,itemType]) {
  	let filtered = allItems.filter((singleItem)=>{
  		return Ember.isEqual(singleItem.get('item_type.content'),itemType);
  	});
  	return filtered.length;
}

export default Ember.Helper.helper(getItemsCount);
