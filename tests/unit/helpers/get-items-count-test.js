import { getItemsCount } from '../../../helpers/get-items-count';
import { module, test } from 'qunit';

module('Unit | Helper | get items count');

// Replace this with your real tests.
test('it works', function(assert) {
  let result = getItemsCount(42);
  assert.ok(result);
});
