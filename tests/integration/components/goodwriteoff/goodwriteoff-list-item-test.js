import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('goodwriteoff/goodwriteoff-list-item', 'Integration | Component | goodwriteoff/goodwriteoff list item', {
  integration: true
});

test('it renders', function(assert) {
  
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });" + EOL + EOL +

  this.render(hbs`{{goodwriteoff/goodwriteoff-list-item}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:" + EOL +
  this.render(hbs`
    {{#goodwriteoff/goodwriteoff-list-item}}
      template block text
    {{/goodwriteoff/goodwriteoff-list-item}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
