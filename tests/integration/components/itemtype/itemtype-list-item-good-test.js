import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('itemtype/itemtype-list-item-good', 'Integration | Component | itemtype/itemtype list item good', {
  integration: true
});

test('it renders', function(assert) {
  
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });" + EOL + EOL +

  this.render(hbs`{{itemtype/itemtype-list-item-good}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:" + EOL +
  this.render(hbs`
    {{#itemtype/itemtype-list-item-good}}
      template block text
    {{/itemtype/itemtype-list-item-good}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
