import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('itemtype/itemtype-main', 'Integration | Component | itemtype/itemtype main', {
  integration: true
});

test('it renders', function(assert) {
  
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });" + EOL + EOL +

  this.render(hbs`{{itemtype/itemtype-main}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:" + EOL +
  this.render(hbs`
    {{#itemtype/itemtype-main}}
      template block text
    {{/itemtype/itemtype-main}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
