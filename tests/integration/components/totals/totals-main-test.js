import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('totals/totals-main', 'Integration | Component | totals/totals main', {
  integration: true
});

test('it renders', function(assert) {
  
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });" + EOL + EOL +

  this.render(hbs`{{totals/totals-main}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:" + EOL +
  this.render(hbs`
    {{#totals/totals-main}}
      template block text
    {{/totals/totals-main}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
