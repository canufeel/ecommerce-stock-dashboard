import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('totals/totals-graph', 'Integration | Component | totals/totals graph', {
  integration: true
});

test('it renders', function(assert) {
  
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });" + EOL + EOL +

  this.render(hbs`{{totals/totals-graph}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:" + EOL +
  this.render(hbs`
    {{#totals/totals-graph}}
      template block text
    {{/totals/totals-graph}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
