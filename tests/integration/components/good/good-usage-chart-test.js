import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('good/good-usage-chart', 'Integration | Component | good/good usage chart', {
  integration: true
});

test('it renders', function(assert) {
  
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });" + EOL + EOL +

  this.render(hbs`{{good/good-usage-chart}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:" + EOL +
  this.render(hbs`
    {{#good/good-usage-chart}}
      template block text
    {{/good/good-usage-chart}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
